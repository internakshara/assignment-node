/*Extend the question 3.2. Write the time taken to copy each file, in the same config file.
 */

var fs = require('fs');
var dir = "config.json";
var content = fs.readFileSync('config.json');
var jsonContent = JSON.parse(content);
var src, dst;
jsonContent.forEach(function(content, i) {
    console.log(jsonContent[i].name);
    src = content.source;
    dst = content.destination;
    var index = dst.lastIndexOf('/');
    var destination = dst.substring(0, index);

    if (!fs.exists(destination)) {
        fs.mkdir(destination);
    }
    var contents = fs.readFileSync(src).toString();
    var start = new Date();
    fs.writeFile(dst, contents, (err) => {
        console.log('written');
    });
    var end = new Date();
    jsonContent[i].timetaken = end.getTime() - start.getTime();
    fs.writeFile(dir, JSON.stringify(jsonContent), (err) => {
        console.log(' time written');
    });
});