/*Repeat question 3.3 with synchronous file operations.*/

var fs = require('fs');
var dir = "config.json";
var content = fs.readFileSync('config.json');
var jsonContent = JSON.parse(content);
var src, dst;
jsonContent.forEach(function(content, i) {
    src = content.source;
    dst = content.destination;
    var index = dst.lastIndexOf('/');
    var destination = dst.substring(0, index);
    if (!fs.existsSync(destination)) {
        fs.mkdir(destination);
    }
    var contents = fs.readFileSync(src).toString();
    var start = new Date();
    fs.writeFileSync(dst, contents);
    var end = new Date();
    jsonContent[i].timetaken = end.getTime() - start.getTime();
    fs.writeFileSync(dir, JSON.stringify(jsonContent));
});