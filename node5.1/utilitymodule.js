module.exports = {
	checkNull : function(value) {
		if(value === null)
			return true;
		else {
			return false;
		}
	},
	checkNan : function(value) {
		return isNaN(value)
	},
	checkBoolean : function(value) {
		if(typeof value === 'boolean') {
			return true;
		} else {
			return false;
		}
	},
	checkFunction : function(value) {
		if(typeof value === 'function') {
			return true;
		} else {
			return false;
		}
	},
	checkObject : function(value) {
		if(typeof value === 'object') {
			return true;
		} else {
			return false;
		}
	}
};