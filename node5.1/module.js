/*Write a utility node module which gives the following api's 
- isNull(object) -> Returns true if the value of object is null.
- isNaN(object) -> Returns true if object is NaN.
- isBoolean(object) -> Returns true if object is either true or false.
- isFunction(object) -> Returns true if object is a Function.
- isObject(value) -> Returns true if value is an Object.
*/


var check = require('./utilitymodule');

console.log("IsNull : ",check.checkNull(null));

console.log("IsNaN : ",check.checkNan("12"));

console.log("IsBoolean : ",check.checkBoolean(true));

console.log("IsFunction : ",check.checkFunction(function() {}));

console.log("IsObject : ",check.checkObject([1, 2, 4]));