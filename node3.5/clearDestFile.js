/*Write a program that clears all the destination files that have been copied. 
Use the same config file to find the copied files and note that you should not delete any other files/folders
 other than the copied ones.*/


var fs = require('fs');

var content = fs.readFileSync('config.json');
var jsonContent = JSON.parse(content);
var src, dst;
jsonContent.forEach(function(readContent) {
    src = readContent.source;
    dst = readContent.destination;
    var index = dst.lastIndexOf('/');
    var destination = dst.substring(0, index);
    if (!fs.existsSync(destination)) {
        fs.mkdir(destination);
    }
    var contents = fs.readFileSync(src).toString();
    fs.writeFileSync(dst, contents);
    fs.unlink(dst);
    console.log("destination file deleted");
})