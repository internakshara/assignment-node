/*Create multiple processes that plays rock, paper, scissors between them and print the results.
Rock defeats scissors, scissors defeat paper and paper defeats the rock.
The main process should evaluate the results of the game and display it.*/


var cp = require('child_process');
var child1 = cp.fork(`${__dirname}/childProcess.js`);
var child2 = cp.fork(`${__dirname}/childProcess2.js`)
var game = ["rock", "paper", "scissor"];
var countChild1 = 0,
    countChild2 = 0;

var ans1, ans2;

child1.on('message', (m) => {
    console.log('child1 : ', game[m]);
    ans1 = m;
});
child2.on('message', (m) => {
    console.log('child2 : ', game[m]);
    ans2 = m;
});

function call(a, b) {
    if (a == 0) {
        if (b == 2) {
            return 1;
        } else if (b == 1) {
            return 2;
        }
    }
    if (b == 0) {
        if (a == 2) {
            return 2;
        } else if (a == 1) {
            return 1;
        }
    }
    if (a == 1) {
        if (b == 2) {
            return 2;
        }
    }
    if (b == 1) {
        if (a == 2) {
            return 1;
        }
    }
}

setInterval(function() {
    if (ans1 != undefined) {
        if (ans1 == ans2) {
            console.log("tie game");
        } else {
            var result = call(ans1, ans2);
            if (result == 1) {
                countChild1++;
            } else if (result == 2) {
                countChild2++;
            }
            console.log("child ", result, " won");
            console.log("child1 --> " + countChild1 + "\nchild2 --> " + countChild2);
        }
        if (countChild1 == 3) {
            console.log("child ", result, " won the match ");
            process.exit();
        } else if (countChild2 == 3) {
            console.log("child ", result, " won the match ");
            process.exit();
        }
    }
}, 1000);