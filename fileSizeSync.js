/*Find the size of a folder by adding up the file sizes of all the files in the folder?
 Get the folder path as an argument - Synchronous*/



var fs = require("fs");
var array = process.argv;
var dir = array[2];

var size = 0;
fs.readdirSync(dir).forEach(file => {
	fs.stat(file, function(err, stats) {
		size += stats.size;
		console.log(size + " bytes");
	});
});