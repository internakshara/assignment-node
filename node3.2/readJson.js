/*Have a config(JSON) file with a list of source files and destination paths.
 Inside the config file, each source file entry will have a destination path.
  The program should copy each of the file from the source, into destination path.
   Note that you will have to create the destination folder if it does not exist.*/


var fs = require('fs');
var content = fs.readFileSync('Files/config.json');
var jsonContent = JSON.parse(content);
var src, dst;
jsonContent.forEach(function(content, index) {
    console.log(content);
    src = content.source;
    dst = content.destination;
    var index = dst.lastIndexOf('/');
    var destination = dst.substring(0, index);
    if (!fs.existsSync(destination)) {
        fs.mkdir(destination);
    }
    var contents = fs.readFileSync(src).toString();
    fs.writeFile(dst, contents, (err) => {
        console.log('written');
    });
});