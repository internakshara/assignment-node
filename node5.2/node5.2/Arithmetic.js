/*Create an api that performs simple arithmetic operations.
 Export these as classes/object and use them.*/

var performArithmetic = require('./ArithmeticModule');

console.log(performArithmetic.add(5, 10));

console.log(performArithmetic.subtract(5, 10));

console.log(performArithmetic.multiply(5, 10));

console.log(performArithmetic.divide(5, 10));