module.exports = {
    add: function(value1, value2) {
        return value1 + value2;
    },
    subtract: function(value1, value2) {
        return value2 - value1;
    },
    multiply: function(value1, value2) {
        return value1 * value2;
    },
    divide: function(value1, value2) {
        return value2 / value1;
    },
}